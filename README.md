# BloxOne Anycast

Anycast lab:
```

                                                         +-----+
                                                         |     |
                                                         |     |
                               +------+                  |     |
                               |      |   172.16.2.0/24  | s2  |
                               |  r2  +------------------+     |
                               |      |                  |     |
                               +---+--+                  |     |
                                   |                     +-----+
                                   |
       +--------+                  |
       |        |                  | 10.1.1.0/24
       | bind9  |                  |
       |        |                  |                     +-----+
       +---+----+                  |                     |     |
           |                       |                     |     |
           |                   +---+--+                  |     |
+------+   |  192.168.1.0/24   |      |                  |     |
|      |   |                   |  r1  |   172.16.1.0/24  | s1  |
|  c1  +---+-------------------+      +------------------+     |
+------+                       +------+                  |     |
                                                         |     |
                                                         +-----+

```

Generate join-token/api-keys from CSP. Run -

```
git clone https://gitlab.com/bloxone/bloxone-anycast.git; cd bloxone-anycast; chmod +x lab
```

Show Lab setup:

```
./lab
```

Run lab:

```
./lab up
```

Stop lab:

```
./lab down
```

Remove lab:

```
./lab clean
```

**Docker Images used:**

*  higebu/vyos-build
*  jpetazzo/dind